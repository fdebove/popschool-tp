var Author = require('../models/author');
var Book = require('../models/book');

// Affiche la liste des auteurs
exports.author_list = function (req, res, next) {

    Author.find()
        .sort([['family_name', 'ascending']]) // Tri par nom de famille
        .exec(function (err, list_authors) {
            if (err) { return next(err); } console.log(list_authors)
            res.render('author_list', { title: 'Liste des auteurs', author_list: list_authors });
        })

};

// Affiche la page de détail pour un auteur.
exports.author_detail = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // author
            Author.findById(req.params.id)
              .exec(function(err, author) {
                if (err) {
                    reject(err);
                }
                resolve(author);
            });
        }),
        new Promise(function(resolve, reject) { // author_books
            Book.find({ 'author': req.params.id }, 'title summary').exec(function(err, author_books) {
                if (err) {
                    reject(err);
                }
                resolve(author_books);
            });
        })
    ]).then(function(results) {

        if (results[0] == null) { // Pas de résultat => Erreur 404
            var err = new Error('Auteur introuvable');
            err.status = 404;
            return next(err);
        }

        res.render('author_detail', {
            title: "Détail d'un auteur",
            author: results[0],
            author_books: results[1]
        });

    }).catch(function(err) {
        next(err);
    });

};

// Display Author create form on GET.
exports.author_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author create GET');
};

// Handle Author create on POST.
exports.author_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author create POST');
};

// Display Author delete form on GET.
exports.author_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author delete GET');
};

// Handle Author delete on POST.
exports.author_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author delete POST');
};

// Display Author update form on GET.
exports.author_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update GET');
};

// Handle Author update on POST.
exports.author_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Author update POST');
};