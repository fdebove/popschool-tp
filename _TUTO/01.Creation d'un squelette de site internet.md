# Création d'un squelette de site internet

> __Prérequis :__ un environnement Node configuré. ==> [Sinon c'est là](_TUTO/00.Configurer un environnement Node.md)      
> __Objectif  :__ être capable de démarrer notre projet avec l'outil *Express Application Generator*.

## Résumé

Ce tuto montre comment vous pouvez créer un *squelette* de site internet avec l'outil *Express Application Generator tool*, que vous pourrez par la suit enrichir avec des infos spécifiques à votre site internet (routes, vues/templates, et base de données).
Dans notre cas, nous utilisons l'outil pour créer la base de notre site internet MaBiliDeQuartier.
Le processus est très simple. Vous devez seulement appeler le générateur en ligne de commande.

## Le générateur d'appli

Pour rappel, on install le générateur comme ça :
```bash
npm install -g express-generator
```

Vous pouvez voir toutes les options du générateur en tapant :
```bash
$ express -h

  Usage: express [options] [dir]


  Options:

        --version        output the version number
    -e, --ejs            add ejs engine support
        --pug            add pug engine support
        --hbs            add handlebars engine support
    -H, --hogan          add hogan.js engine support
    -v, --view <engine>  add view <engine> support (dust|ejs|hbs|hjs|jade|pug|twig|vash) (defaults to jade)
        --no-view        use static html instead of view engine
    -c, --css <engine>   add stylesheet <engine> support (less|stylus|compass|sass) (defaults to plain css)
        --git            add .gitignore
    -f, --force          force on non-empty directory
    -h, --help           output usage information
```

## Créer le projet

Pour ce tuto, nous allons créer notre appli nommée `popschool-tp` avec le moteur de vue **Pug** et du CSS pur.
Pour commencer, aller dans le dossier où vous voulez créer le projet et lancer l'outil en ligne de commande :

```bash
express popschool-tp --view=pug
```

Le générateur va créer et lister les fichiers du projet :
```bash

   create : public/
   create : public/javascripts/
   create : public/images/
   create : public/stylesheets/
   create : public/stylesheets/style.css
   create : routes/
   create : routes/index.js
   create : routes/users.js
   create : views/
   create : views/error.pug
   create : views/index.pug
   create : views/layout.pug
   create : app.js
   create : package.json
   create : bin/
   create : bin/www

   install dependencies:
     $ npm install

   run the app:
     $ DEBUG=popschool-tp:* npm start

```

A la fin, vous voyez les instructions pour installer les dépendances du projet (listées dans le fichier `package.json`) et comment lancer l'appli. (les instructions au dessus sont pour macOS; sur Linux/Windows elles peuvent être différentes).

> Commit GIT : 5c35dd1916f6fe61b9953c8614f5ad25eba9f0a0

## Lancer le squelette de site internet

A ce niveau, nous avons un squelette complet de projet. Le site internet ne fera pas grand chose mais il tourne.

1. Installer les dépendances (la commande `install` récupérera toutes les dépendances listées dans le fichier `package.json`)
```bash
cd popschool-tp
npm install
```

> Commit GIT : 35c721b783bf239e4e53b3f5e1d79223c3538f17

2. On lance l'appli
   - Sur Windows :
     ```bash
     SET DEBUG=popschool-tp:* & npm start
     ```
   - Sur macOs ou Linux :
     ```bash
     DEBUG=popschool-tp:* npm start
     ```

3. Charger la page dans le navigateur : http://localhost:3000/

Vous verrez une page comme ceci :

![Première page](/_TUTO/images/tuto1.jpeg)

Avec la variable `DEBUG` dans la ligne de commande, vous voyez sur le terminal les pages et les ressources demandées :
```bash
> DEBUG=popschool-tp:* npm start

> popschool-tp@0.0.0 start /Users/jbanety/Sites/popschool-tp
> node ./bin/www

  popschool-tp:server Listening on port 3000 +0ms
GET / 200 2025.760 ms - 170
GET /stylesheets/style.css 200 92.338 ms - 111
GET /favicon.ico 404 112.435 ms - 1152
```

## Permettre le redémarrage du serveur en cas de changements dans les fichiers

Chaque changement que vous faites sur votre site internet Express n'est pas visible tant que vous ne redémarrer pas votre appli.
Pour le faire automatiquement, on utilise `nodemon`. Cet outil est généralement installé globalement mais ici nous l'installons comme dépendances de développement (`--save-dev`).

```bash
npm install --save-dev nodemon
```

> Commit GIT : 4e3a66ad89c3ccdf413652402bf50cc7fd65ff3c

Si vous ouvrez le fichier `package.json` de votre projet, vous verrez une nouvelle section :
```json
"devDependencies": {
  "nodemon": "^1.18.9"
}
```

Comme l'outil n'est pas installé globalement, on peut pas l'exécuter directement en ligne de commande. On doit créer un script NPM.
Chercher la section `scripts` de votre fichier `package.json`.
Au début, elle ne contient qu'une seule ligne avec la commande `start`.
Ajouter une commande `devstart` comme ceci :
```json
"scripts": {
  "start": "node ./bin/www",
  "devstart": "nodemon ./bin/www"
},
```

> Commit GIT : bfb62d2cf9314684ad771fe43fd6ac68e4f003bb

Maintenant vous pouvez démarrer le serveur quasiment comme avant mais avec `devstart` :
- Sur Windows :
 ```bash
 SET DEBUG=popschool-tp:* & npm run devstart
 ```
- Sur macOs ou Linux :
 ```bash
 DEBUG=popschool-tp:* npm run devstart
 ```

## Le projet généré

### Structure des dossiers

Le fichier `package.json` définit les dépendances de votre appli et d'autres infos.
Il définit aussi le script de démarrage qui appel le point d'entrée de l'appli, le fichier Javascript `/bin/www`.
Ce fichier définit quelques gestions d'erreur et charge `app.js` pour faire le reste du boulot.
Les routes de l'appli sont stockées dans des modules séparés dans le dossier `/routes`.
Les templates sont stockés dans le dossier `/views`

```
/popschool-tp
    app.js
    /bin
        www
    package.json
    /node_modules
        [environ 4500 sous-dossiers et fichiers]
    /public
        /images
        /javascripts
        /stylesheets
            style.css
    /routes
        index.js
        users.js
    /views
        error.pug
        index.pug
        layout.pug
```

### package.json

Le fichier `package.json` définit les dépendances de l'appli et d'autre infos :
```json
{
  "name": "popschool-tp",
  "version": "0.0.0",
  "private": true,
  "scripts": {
    "start": "node ./bin/www",
    "devstart": "nodemon ./bin/www"
  },
  "dependencies": {
    "cookie-parser": "~1.4.3",
    "debug": "~2.6.9",
    "express": "~4.16.0",
    "http-errors": "~1.6.2",
    "morgan": "~1.9.0",
    "pug": "2.0.0-beta11"
  },
  "devDependencies": {
    "nodemon": "^1.18.9"
  }
}
```

Les dépendances incluent le paquet *express* et *pug*. En plus, nous avons les packages suivants :
- cookie-parser : utilisé pour récupérer les cookies et les stocker dans `req.cookies`
- debug : un petit outil de débuggage
- morgan : un middleware permettant de logguer les requêtes HTTP

### Fichier www

Le fichier `/bin/www` est le point d'entrée de l'appli ! La toute première chose qu'il fait est de récupérer (`require()`) le "vrai" point d'entrée de l'application (`app.js`).

```js
#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
```

`require()` est une fonction globale de Node qui permet d'importer les modules dans le fichier courant. C'est un peu comme les `import` en Python, PHP ou Java.

Le reste du code met en place un serveur HTTP Node avec `app` avec un port spécifique (3000) et démarre l'écoute.
Vous n'avez pas vraiment besoin de savoir comment cela fonctionne.

### app.js

Ce fichier crée un objet d'appli `express` (nommé `app` par convention), configure l'appli avec différents paramètres et middleware et exporte l'appli du module.
Le code en dessous présente les parties du fichier qui crée et exporte l'objet `app` :
```js
[...]
var express = require('express');
[...]
var app = express();
[...]
module.exports = app;
```

Si on retourne dans le fichier `www`, c'est cet objet `module.exports` qui est donné à la fonction `require()` quand le fichier est importé.

> On pourra détailler le fichier app.js en cours.

### Routes

Le fichier `/routes/users.js` est montré ci-dessous. Tous les fichiers de route sont similaires.

1. Il charge le module `express` et l'utilise pour avoir l'objet `express.Router`.
2. Il spécifit une route sur cet objet
3. Il exporte le routeur du module (ce qui permet de l'importer dans `app.js`)

```js
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
```

La route définit un callback, une fonction de retour, censée être appellée quand on reçoit la requête HTTP `GET` quand le bon modèle est détecté.
Le modèle correspondant est la route spécifiée quand le module est importé (`/users`) en plus de ce qui est définit dans le fichier (ici `/`).
Donc, la route sera utilisée quand l'URL `/users/` sera reçue.

> A essayer dans le navigateur : http://localhost:3000/users/

### Vues et templates

Les vues (templates) sont stockées dans le dossier `/views` (comme décrit dans `app.js`) et ont l'extension `.pug`.
La méthode `Response.render()` est utilisée pour effectuer le rendu d'un template en particulier.

## Relevez le défit

Créez une nouvelle route dans `/routes/users.js` qui affichera le text "Popschool" à l'adresse `/users/popschool`.
On peut la tester en démarrant le serveur et en visitant http://localhost:3000/users/popschool dans votre navigateur.

> Commit GIT : 3a9cfcb90e73810c497b70616c137c55a18db169

___

[Partie 2 : Utiliser une base de données](/_TUTO/02.Utiliser une base de donnees.md)
